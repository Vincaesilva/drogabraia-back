package br.com.drogabraia.enums;

public enum GenderEnum {
    MASCULINO,
    FEMININO,
    NAO_BINARIO
}
