package br.com.drogabraia.enums;

public enum AdministrationRouteEnum {
    ORAL,
    INJETAVEL,
    TOPICA,
    SUBLINGUAL,
    INALATORIA,
    OUTRA
}
