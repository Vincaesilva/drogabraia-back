package br.com.drogabraia.repository;

import br.com.drogabraia.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CartRepository extends JpaRepository<Cart, Long> {

}
