package br.com.drogabraia.repository;

import br.com.drogabraia.model.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {

    // --Commented out by Inspection (19/07/2023 13:11):Optional<Manufacturer> findByRazaoSocial(String name);

    Optional<Manufacturer> findByCnpj(String cnpj);

    boolean existsByCnpj(String cnpj);

}
