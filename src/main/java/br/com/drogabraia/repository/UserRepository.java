package br.com.drogabraia.repository;

import br.com.drogabraia.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findById(Long id);

    boolean existsByCpf(String cpf);

    boolean existsByEmail(String email);

    Optional<User> findByEmailAndNumero(String email, String numero);
}
